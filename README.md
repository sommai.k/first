# Hello

## Title

- list 1
- list 2

### Order list

1. One
1. Two
1. Three
1. Four

```typescript
let x: number = 0;
console.log(x);
```

```python

print('xxxxx');
```

```java

public static void main(String[] args) {

}
```

this is a **bold**

this is a _text_

```
docker stop some-nginx
docker rm some-nginx
docker run --name some-nginx \
-p 80:80 \
-v /Users/sommaik/Workspace/first/html:/usr/share/nginx/html \
-d \
nginx
```

```
docker build -t first .

docker stop some-nginx
docker rm some-nginx

docker run --name some-nginx \
-p 80:80 \
-d \
first


docker tag first registry.gitlab.com/sommai.k/first

docker push registry.gitlab.com/sommai.k/first

docker tag first registry.gitlab.com/sommai.k/first:1.0
docker push registry.gitlab.com/sommai.k/first:1.0


docker build -t registry.gitlab.com/sommai.k/first:2.0 .
docker push registry.gitlab.com/sommai.k/first:2.0
docker tag registry.gitlab.com/sommai.k/first:2.0 registry.gitlab.com/sommai.k/first:latest
docker push registry.gitlab.com/sommai.k/first:latest
```

```
https://github.com/sommaik/devops
```

```
docker container prune
docker image prune
docker system prune

docker-compose up -d
```

### Windows

```
docker-machine create --driver hyperv vm1
```

### Mac or Linux

```
docker-machine create vm1

docker-machine env vm1

docker-machine active

docker-machine ip vm1

docker swarm init --advertise-addr=192.168.99.185

docker-machine env vm2

docker-machine active

docker swarm join --token SWMTKN-1-5vz5qpzp4jifozfof2sr3sd2c1teys0915mi77ikwddxdpqpfo-7b1tq10lmxto3u9acmrbwdqaf 192.168.99.185:2377


docker service rm first

docker service create \
--name first \
--replicas 2 \
--constraint "node.role != manager" \
--publish 8080:80 \
--with-registry-auth \
registry.gitlab.com/sommai.k/first:latest

docker service scale first=10

docker service update first \
--with-registry-auth \
--image registry.gitlab.com/sommai.k/first:1.0


docker-machine env vm4

docker logs jenkins

docker-machine env vm5


docker run --name deploy \
-v /var/run/docker.sock:/var/run/docker.sock \
-d \
adriagalin/jenkins-jnlp-slave \
-url http://192.168.99.188:8080 4f4df502152f636e11ce23c88272ba466c5b6198d84b66fad1a31d4e1376f091 vm5


docker-machine env vm1

docker run --name deploy \
-v /var/run/docker.sock:/var/run/docker.sock \
-d \
adriagalin/jenkins-jnlp-slave \
-url http://192.168.99.188:8080 ec7040a32e2d70936539dfe40b4283587417232c1e6741211fecc573b50fbf4f vm1


```
